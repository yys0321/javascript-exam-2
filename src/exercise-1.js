const getCommonItems = (array1, array2) => {
  return array1.map((item) => array2.includes(item) ? item : undefined).filter(Boolean)
};

export default getCommonItems;