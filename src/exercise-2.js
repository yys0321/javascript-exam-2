const getTheExactNumber = (numbers) => {
  return numbers.reduce((max, current) => current % 3 === 0 && current > max ? current : max)
}

export default getTheExactNumber;